<?php
namespace Uxin603s;
class Container{
	private static $cache=[];
	private static $cache_func=[];
	public static function bind($name,$callback){
		static::$cache_func[$name]=$callback;
	}
	public static function get($name,$object=null){
		if(!isset(static::$cache[$name])){
			if(isset(static::$cache_func[$name])){
				static::$cache[$name]=call_user_func(static::$cache_func[$name]);
			}else{
				static::$cache[$name]=static::set($name,$object);
			}
		}
		return static::$cache[$name];
	}
	public static function set($name,$object=null){
		if(is_null($object)){
			$args=[];
			$ReflectionClass=new \ReflectionClass($name);
			if($Constructor=$ReflectionClass->getConstructor()){
				$parameters=$Constructor->getParameters();
				foreach ($parameters as $param) {
					if($class=$param->getClass()){
						$class_name=$class->getName();
						$args[]=static::get($class_name);
					}else{
						throw new Exception("不能DI非class的東西");
					}
				}
				$object=$ReflectionClass->newInstanceArgs($args);
			}else{
				$object=new $name;
			}
		}
		static::$cache[$name]=$object;
		return $object;
	}
}
